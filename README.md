# Ex-Alert

Wifi MAC triangulation with google map overlay.


Inspired by previous projects such as CreepyDOL (?) and SNOOPY. During my college career, I cobbled together a project with the vision of using disposable cheap chips to follow 
in those footsteps. 
Especially when I found out that the kits where being sold for over 300 dollars!

Given my broke college status, and the fact the ESP-8266 had just been introduced to the world, it seemed like a match made in heaven.

Unfortunately, my C skills and lack of any previous electronic gadgetry abilities left the project in a 'demo' status using a python script meant for 3 laptops using a 
wireless card with suitable injection capabilities like the ALPHA cards used in pretty much any wireless hacking guide you can come across.


The concept is pretty simple. The code would be loaded onto 'watch towers' (ESP chips) and when placed on the ground the GPS coordinates would be marked on a phone app which would
send said coordinates and throw them to the backend SQL server. At this point I'd found mention of a mesh-network firmware for the chips and had the idea to use that to have them 
coordinate with each other until one came into range of an open access point (Zigbee already did it!) 

From here the chips would be programmed to do a dump of wireless MAC addresses in the area, and tag the relevant information (airo-dump-ng style) along with the
RSSI(power of the signal hitting the chip) and throw it to the backend server.


Finally, the end-user could log into the web portal, and using a web front end simply query the back end using (what i thought at the time) are super keen SQL statements to display 
MAC addresses being searched for. A "simple" triangulation formula calculation after that, and a little help from google map API, and BAM you get a nice circle overlay of the aprox 
location of said MAC address spotted.

Finally, My partner was able to get ahold of some of the local talent and produce some custom icons and art for the front end. 


Hopefully I'll get to re-do this readme later and clarify use-case and other items, but I am eager to upload and post before I lose my nerve at the project not being good enough 
to release given it's unfinished state.



Here you go cyber-community. Hopefully you accept this humble offering.